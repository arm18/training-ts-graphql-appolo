import express from 'express';
import graphqlHTTP from 'express-graphql';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import { schema } from './data/schema';

const app = express();

app.get('/', (req, res) => {
  res.send('gql is smt')
})

app.use('/graphql', graphqlHTTP({
  schema: schema,
  graphiql:  true
}));

app.listen(8080, () => console.log("Running on port localhost:8080/graphql"))

// const PORT = process.env.PORT || 8080;
// const HOST = process.env.HOST || 'localhost';

// const app = express();
// app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
// app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));

// app.listen(PORT, () => {
//   console.log(`Server started at: http://${HOST}:${PORT}/graphiql`); // eslint-disable-line no-console
// });