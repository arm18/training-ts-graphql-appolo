import gql from 'graphql-tag';
import { filter , has} from 'lodash';
import { Schema } from 'mongoose';

const isRequiredField = (field: any) => field.type.kind === 'NonNullType';

const isFieldArray = (field) => field.type.kind === 'ListType';

const getFieldType = (type) => {
    switch (type) {
        case 'String':
            return 'String';
        case 'Number':
            return 'Number';
        case 'ID':
            return 'ObjectId';
        default:
            return type;
    }
};

const getMongooseType = (field, Types) => {
      //TODO:: Change
      const type = has(field, 'type.name') ? field.type.name.value : field.type.type.name.value;
      const parsedType = getFieldType(type);
      return isFieldArray(field) ? [parsedType] : parsedType;
};

const parseSchema = (fields) => {
    return fields.reduce((acc, field) => {
        return {
            ...acc,
            [field.name.value]: {
                type: getMongooseType(field, Schema.Types),
                required: isRequiredField(field)
            }
        }
    }, {});
};

const parseModel = ({ fields, name } : any) => {
    return {
        collection: name.value,
        schema: parseSchema(fields)
    }
};

const wql = (...args) => {
    const parsedSchema = gql`${args[0][0]}`;
    return filter(parsedSchema.definitions, { kind: 'ObjectTypeDefinition' }).map(parseModel);
};

console.log(JSON.stringify(wql
    `
  directive @computed(value: String) on FIELD_DEFINITION

  type Friend {
    id: ID
    firstName: String
    lastName: String
    gender: Gender
    language: String
    email: String
    contacts: [Contact]
   
  }

  type Alien {
    id: ID
    firstName: String
    lastName: String
    planet: String
  }

  type Contact {
    firstName: String
    lastName: String
  }

  enum Gender {
    MALE,
    FEMALE,
    OTHER
  }

  type Query {
    getOneFriend(id: ID!): Friend
    getAlines: [Alien]
  }

  input FriendInput {
    id: ID
    firstName: String!
    lastName: String
    gender: Gender
    language: String
    email: String
    contacts: [ContactInput]
  }

  input ContactInput {
    firstName: String
    lastName: String
  }
   
  type Mutation {
    createFriend(input: FriendInput): Friend
    updateFriend(input: FriendInput): Friend
    deleteFriend(id: ID!): String
  }
`   ));