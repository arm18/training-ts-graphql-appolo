import { GraphQLServer } from 'graphql-yoga';

let links = [{
  id: 'link-0',
  url: 'www.howtographql.com',
  description: 'Fullstack tutorial for GraphQL'
}]

// const typeDefs = `
//   type Query {
//     info: String!
//     feed: [Link!]!
//   }

//   type Mutation {
//     post(url: String!, description: String!): Link!
//   }

//   type Link {
//     id: ID!
//     description: String!
//     url: String!
//   }
// `

let findById = (id, arr) => {
  return links.find((elem) => elem.id === id)
}

let idCount = links.length;
const resolvers = {

  Query: {
    info: () => `This is the API of a Hackernews Clone`,
    feed: () => links,
    link: (root, {id}) => findById(id, links)
  },

  Mutation: {
    post: (parent, args) => {
      const link = {
        id: `link-${idCount++}`,
        description: args.description,
        url: args.url
      }
      links.push(link);
      return link;
    },
    updateLink: (parent, {id, url, description}) => {    
      let elem = findById(id, links);
      if(elem !== null) {
        elem.url = url,
        elem.description = description
      }
      return elem;
    },
    deleteLink: (parent, {id}) => {
      let element = findById(id, links);
      links.splice(links.indexOf(element), 1);
      console.log(links);  
      return `${id} is deleted`;
    }
  }

  // Link: {
  //   id: (parent) => parent.id,
  //   description: (parent) => parent.description,
  //   url: (parent) => parent.url
  // }
}

const server = new GraphQLServer({
  typeDefs: './src/schema.graphql',
  resolvers
})

server.start(() => console.log(`Server is running on http://localhost:4000`));