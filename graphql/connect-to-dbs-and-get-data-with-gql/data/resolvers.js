// class Friend {
//   constructor(id, { firstName, lastName, gender, language, email, contacts }) {
//     this.id = id;
//     this.firstName = firstName;
//     this.lastName = lastName;
//     this.gender = gender;
//     this.language = language;
//     this.email = email;  
//     this.contacts = contacts
//   }
// }
// const friendDAtabase = {};

import mongoose from 'mongoose';
import { Friends, Aliens } from './dbConnectors';
import { rejects } from 'assert';
export const resolvers = {
  Query: {
    getOneFriend: (root, { id }) => {
      return new Promise((resolve, reject) => {
        Friends.findById(id, (err, friend) => {
          if (err) {
            reject(err)
          } 
          else resolve(friend)
        })
      })
    },
    getAlines: () => {
      return Aliens.findAll();
    }
  },
  Mutation: {
    createFriend: (root, { input }) => {
      // let id = require('crypto').randomBytes(10).toString('hex');
      // friendDAtabase[id] = input;
      // return new Friend(id, input);
      const newFriend = new Friends({
        firstName: input.firstName,
        lastName: input.lastName,
        gender: input.gender,
        language: input.language,
        email: input.email,
        contacts: input.contacts,
      });

      newFriend.id = newFriend._id;

      return new Promise((resolve, reject) => {
        newFriend.save((err) => {
          if (err) reject(err)
          else resolve(newFriend)
        })
      })
    },
    updateFriend: (root, { input }) => {
      return new Promise(( resolve, reject ) => {
        Friends.findOneAndUpdate({ _id: input.id }, input, {new: true}, (err, friend) => {
          if (err) reject(err)
          else resolve(friend)
        })
      })
    },
    deleteFriend: (root, { id }) => {
      return new Promise(( resolve, reject ) => {
        Friends.remove({ _id: id}, (err) =>{
          if (err) reject(err)
          else resolve("deleted")
        })
    })
  }
}
}; 
