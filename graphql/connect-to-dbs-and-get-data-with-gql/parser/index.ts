import { 
  buildASTSchema,
  parse
} from 'graphql';

import _ from 'lodash';
import * as schema from '../data/schema';

// namespace Parse {
//   const Parser = ()
// }

enum RESOLVERS {
  Query,
  Mutation,
  Subscription
}


function makeASTfromGraphQlScheme(scheme: string): object {
  return parse(scheme);
}

interface Name {
  kind: string
  value: string
}
interface Type{
  kind: string
  name?: Name | any,
  type?: Type | any
}

function convertTypeToMongooseType({ kind, name, type}: Type): string {
  if(kind === "ListType"){
    console.log("Array of ", type.name.value)
  }
  else if(kind === "NamedType") {
    console.log("===", name.value)
  }
  return ""
}


function convertGraphQlToMongoose(graphqlScheme: string): object {
  let AST = makeASTfromGraphQlScheme(graphqlScheme);
  // console.log(Object.values(AST)[1]);
  
  _.forEach(Object.values(AST)[1], (value, key) => {
    const { value: typeName } = value.name;
    if(value.kind === "ObjectTypeDefinition" && !Object.values(RESOLVERS).includes(typeName) && typeName === "Friend"){ //must be universal ??

        // console.log( value.fields );     
      _.each(value.fields, (value, key) => {
        // const { fieldName } = value.name.value;
        // if( value.type.kind )   
        // console.log( value.name.value, value.type); 
        convertTypeToMongooseType(value.type);    
      })
    }    
  })
  return {}
}

convertGraphQlToMongoose(schema.typeDefs);







